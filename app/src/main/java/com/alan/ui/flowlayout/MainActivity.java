package com.alan.ui.flowlayout;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.alan.ui.flow.FlowLayout;
import com.alan.ui.flowlayout.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private  ActivityMainBinding binding;
    private static List<String> array = new ArrayList<>();
    static {
        array.add("大海");
        array.add("东方明珠");
        array.add("乡村爱情");
        array.add("陀枪师姐");
        array.add("爱情公寓");
        array.add("我爱北京天安门");
        array.add("北京人在纽约");
        array.add("王大海");
        array.add("收音机");
        array.add("网易云音乐");
        array.add("宝马320");
        array.add("大海");
        array.add("东方明珠");
        array.add("乡村爱情");
        array.add("陀枪师姐");
        array.add("爱情公寓");
        array.add("我爱北京天安门");
        array.add("北京人在纽约");
        array.add("王大海");
        array.add("收音机");
        array.add("网易云音乐");
        array.add("宝马320");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        binding.layoutTagContainer.setData(array, new FlowLayout.OnCreateViewListener<String>() {
            @Override
            public View onCreateViewListener(String s) {
                TextView textView = new TextView(MainActivity.this);
                textView.setBackgroundResource(R.drawable.shape_item);
                textView.setGravity(Gravity.CENTER);
                int left = dip2px(20);
                textView.setPadding(left, 0, left, 0);
                ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, dip2px(50));
                marginLayoutParams.rightMargin = dip2px(15);
                marginLayoutParams.topMargin = dip2px(20);
                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,dip2px(15));
                textView.setLayoutParams(marginLayoutParams);
                textView.setText(s);
                textView.setTextColor(Color.WHITE);
                textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
                    }
                });
                return textView;
            }
        });
    }



    public static int dip2px(float dpValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}