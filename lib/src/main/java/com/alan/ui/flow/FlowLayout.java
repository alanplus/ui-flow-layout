package com.alan.ui.flow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class FlowLayout extends ViewGroup {


    public FlowLayout(Context context) {
        super(context);
    }

    public FlowLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int count = getChildCount();
        if (count == 0) {
            return;
        }
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int lineWidth = 0;
        int lineHeight = 0;

        int width = 0;//需要记录的宽度
        int height = 0;//需要记录的高度

        for (int i = 0; i < count; i++) {
            View view = getChildAt(i);
            measureChild(view, widthMeasureSpec, heightMeasureSpec);
            MarginLayoutParams lp = (MarginLayoutParams) view.getLayoutParams();
            int childWidth = view.getMeasuredWidth() + lp.leftMargin + lp.rightMargin;
            int childHeight = view.getMeasuredHeight() + lp.topMargin + lp.bottomMargin;

            if (lineWidth + childWidth > widthSize) {
                width = Math.max(lineWidth, childWidth);//这种情况就是排除单个标签很长的情况
                lineWidth = childWidth;//开启新行
                height += lineHeight;//记录总行高
                lineHeight = childHeight;//因为开了新行，所以这行的高度要记录一下
            } else {
                lineWidth += childWidth;
                lineHeight = Math.max(height, childHeight); //记录行高
            }
            // 如果是最后一个，则将当前记录的最大宽度和当前lineWidth做比较
            if (i == count - 1) {
                width = Math.max(width, lineWidth);  //宽度
                height += lineHeight;  //
            }
        }
        int w = widthMode == MeasureSpec.EXACTLY ? widthSize : width;
        int h = heightMode == MeasureSpec.EXACTLY ? heightSize : height;
        setMeasuredDimension(w, h);
    }

    @Override
    @SuppressLint("DrawAllocation")
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int childCount = getChildCount();
        if (childCount == 0) {
            return;
        }

        List<List<View>> tempViews = new ArrayList<>();
        List<Integer> tempLineHeight = new ArrayList<>();
        int width = getWidth();//得到当前控件的宽度（在onMeasure方法中已经测量出来了）
        List<View> lineViews = new ArrayList<>();

        int lineWidth = 0;  //行宽
        int lineHeight = 0; //总行高
        for (int i = 0; i < childCount; i++) {
            View child = getChildAt(i);
            MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();//得到属性参数
            int childWidth = child.getMeasuredWidth();
            int childHeight = child.getMeasuredHeight();
            // 如果已经需要换行
            if (childWidth + lp.leftMargin + lp.rightMargin + lineWidth > width)  //大于父布局的宽度
            {
                // 记录这一行所有的View以及最大高度
                tempLineHeight.add(lineHeight);
                // 将当前行的childView保存，然后开启新的ArrayList保存下一行的childView
                tempViews.add(lineViews);
                lineWidth = 0;// 重置行宽
                lineViews = new ArrayList<>();
            }
            /**
             * 如果不需要换行，则累加
             */
            lineWidth += childWidth + lp.leftMargin + lp.rightMargin;
            lineHeight = Math.max(lineHeight, childHeight + lp.topMargin + lp.bottomMargin);
            lineViews.add(child);
        }

        // 记录最后一行  (因为最后一行肯定大于父布局的宽度，所以添加最后一行是必要的)
        tempLineHeight.add(lineHeight);
        tempViews.add(lineViews);

        int left = 0;
        int top = 0;
        int lineNums = tempViews.size();
        for (int i = 0; i < lineNums; i++) {
            // 每一行的所有的views
            lineViews = tempViews.get(i);
            // 当前行的最大高度  每一行的高度都相同  所以使用（i+1）进行设置高度
//            lineHeight = (i + 1) * mLineHeight.get(i);
            for (int j = 0; j < lineViews.size(); j++) {
                View lineChild = lineViews.get(j);
                if (lineChild.getVisibility() == View.GONE) {
                    continue;
                }
                MarginLayoutParams lp = (MarginLayoutParams) lineChild.getLayoutParams();
                //开始画标签了。左边和上边的距离是要根据累计的数确定的。
                int lc = left + lp.leftMargin;
                int tc = top + lp.topMargin;
                int rc = lc + lineChild.getMeasuredWidth();
                int bc = tc + lineChild.getMeasuredHeight();
                lineChild.layout(lc, tc, rc, bc);
                left += lineChild.getMeasuredWidth() + lp.rightMargin + lp.leftMargin;
            }
            left = 0;//将left归零
            top += tempLineHeight.get(i);
        }
    }

    public <T> void setData(List<T> dataList, OnCreateViewListener<T> listener) {
        if (null == dataList || dataList.size() == 0 || listener == null) {
            return;
        }
        removeAllViews();
        for (T t : dataList) {
            addView(listener.onCreateViewListener(t));
        }
    }

    public interface OnCreateViewListener<T> {
        View onCreateViewListener(T t);
    }
}
